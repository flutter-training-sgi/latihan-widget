import 'package:flutter/material.dart';
import '../screens/home_screen.dart';
import '../screens/grid_screen.dart';
import '../screens/list_screen.dart';
import '../screens/about_screen.dart';

class AppRoute {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // jika ingin mengirim argument
    // final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case '/grid':
        return MaterialPageRoute(builder: (_) => GridScreen());
      case '/list':
        return MaterialPageRoute(builder: (_) => ListScreen());
      case '/about':
        return MaterialPageRoute(builder: (_) => AboutScreen());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: Text("Error")),
        body: Center(child: Text('Error page')),
      );
    });
  }
}