import 'package:flutter/material.dart';

class BottomNavScreen extends StatelessWidget {
  const BottomNavScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: BottomNav()
      );
  }
}

class BottomNav extends StatefulWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  State<BottomNav> createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  int _selectedNavbar = 0;

  void _changeSelectedNavBar(int index) {
    setState(() {
      _selectedNavbar = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Belajar Flutter'),
        ),
        body: Center(
          child: Text("Tab Index yang aktif : $_selectedNavbar"),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: "Beranda"
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.assignment),
                label: "Pesanan"
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.mail),
                label: "Inbox"
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: "Akun"
            ),
          ],
          currentIndex: _selectedNavbar,
          selectedItemColor: Colors.green,
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: true,
          onTap: _changeSelectedNavBar,
        ),
      );
  }
}

