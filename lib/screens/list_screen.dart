import 'package:flutter/material.dart';

class ListScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Judul")
        ),
      body: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(15),
              child: Text('Flutter Widget: Penggunaan ListView Class',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold)
              ),
            ),
            Container(
              padding: EdgeInsets.all(15),
              child: Text(
                  '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nisi scelerisque eu ultrices vitae auctor. Facilisis mauris sit amet massa. Fringilla phasellus faucibus scelerisque eleifend donec. Morbi tincidunt augue interdum velit euismod in pellentesque massa placerat. Pellentesque habitant morbi tristique senectus et netus. Tincidunt eget nullam non nisi est. Amet venenatis urna cursus eget nunc scelerisque viverra. Cursus sit amet dictum sit amet. Imperdiet nulla malesuada pellentesque elit eget gravida cum sociis natoque
Pharetra convallis posuere morbi leo urna molestie at. Consequat mauris nunc congue nisi vitae. Integer malesuada nunc vel risus. Vitae sapien pellentesque habitant morbi. Suspendisse faucibus interdum posuere lorem ipsum dolor sit amet. Massa tempor nec feugiat nisl pretium fusce. Amet consectetur adipiscing elit ut. Etiam erat velit scelerisque in dictum non consectetur a. Vitae suscipit tellus mauris a diam maecenas. Lectus arcu bibendum at varius vel. Adipiscing at in tellus integer feugiat scelerisque. Pulvinar neque laoreet suspendisse interdum consectetur libero. Sit amet est placerat in egestas erat imperdiet.
Urna duis convallis convallis tellus id interdum. Condimentum lacinia quis vel eros donec. Aliquet enim tortor at auctor urna nunc. Fermentum et sollicitudin ac orci phasellus egestas. Interdum varius sit amet mattis vulputate enim nulla aliquet porttitor. Sed felis eget velit aliquet sagittis id consectetur purus. Ut faucibus pulvinar elementum integer enim neque volutpat. Magna etiam tempor orci eu lobortis elementum nibh tellus molestie. Sit amet porttitor eget dolor morbi non arcu risus quis. Enim facilisis gravida neque convallis a cras semper auctor. Dolor sed viverra ipsum nunc aliquet bibendum enim. Neque volutpat ac tincidunt vitae semper quis lectus nulla. Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Et ligula ullamcorper malesuada proin. Scelerisque felis imperdiet proin fermentum leo vel. Eu ultrices vitae auctor eu. Porta non pulvinar neque laoreet suspendisse interdum consectetur libero. Purus non enim praesent elementum facilisis. Nullam vehicula ipsum a arcu. Quam adipiscing vitae proin sagittis nisl rhoncus.
Amet cursus sit amet dictum. Ante metus dictum at tempor commodo ullamcorper a lacus. Ut pharetra sit amet aliquam. Purus sit amet volutpat consequat mauris. Quis risus sed vulputate odio ut enim. Tortor consequat id porta nibh venenatis cras sed. Et netus et malesuada fames. Odio tempor orci dapibus ultrices. Turpis egestas maecenas pharetra convallis. Ultrices neque ornare aenean euismod elementum nisi quis eleifend quam. Fermentum iaculis eu non diam phasellus vestibulum lorem sed risus.
Tincidunt augue interdum velit euismod in pellentesque massa placerat. Sed velit dignissim sodales ut eu sem. Egestas erat imperdiet sed euismod. Sem fringilla ut morbi tincidunt. Id cursus metus aliquam eleifend mi in nulla posuere sollicitudin. Amet consectetur adipiscing elit ut aliquam. Id interdum velit laoreet id donec ultrices tincidunt arcu. Lacus vestibulum sed arcu non. Ut eu sem integer vitae justo eget. Nam libero justo laoreet sit amet cursus sit amet dictum. Id diam vel quam elementum pulvinar etiam non quam.                  
                  ''',
                  style: TextStyle(fontSize: 16)
              ),
            ),
          ]
      ),
    );
  }
}
