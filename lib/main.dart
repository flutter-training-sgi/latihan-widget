import 'package:flutter/material.dart';
import 'route/app_route.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: AppRoute.generateRoute,
  ));
}
